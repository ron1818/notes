### notes ###
- no `ifconfig` but has to use `ip addr`

### install gps ###
[reference](http://www.96boards.org/forums/topic/gps-software/)
```bash
sudo apt-get install gnss-gpsd gpsd gpsd-clients libgps-dev
# start service
systemctl start qdsp-start
# or
sudo systemctl start qdsp-start.service
# test functioning:
xgps # gui for gps monitoring
gpsmon # cmdline for gps monitoring
```

### pinout ###
![pinout img](https://az835927.vo.msecnd.net/sites/iot/Resources/images/PinMappings/DB_Pinout.png)

### opencv ###
```bash
sudo apt-get install build-essential
sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
sudo apt-get install python-dev libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev
sudo apt-get install python-numpy python-scipy
sudo apt-get install libopencv-dev python-opencv
# check
python
>>> import cv2
>>> exit()
```